const swaggerJsdoc = require('swagger-jsdoc');

const options = {
    swaggerDefinition: {
        openapi: '3.0.0',
        // Like the one described here: https://swagger.io/specification/#infoObject
        info: {
            title: 'Survey API',
            version: '1.0.0',
            description: 'The api documentation for the anonymous survey application',
        },
        servers: [
            {
                url: '/api',
                description: 'Default server',
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT",
                }
            },
            schemas: {
                User: {
                    type: 'object',
                    properties: {
                        _id: {
                            type: 'string',
                            format: 'email',
                        },
                    }
                },
                Survey: {
                    type: 'object',
                    properties: {
                        _id: {
                            type: 'string',
                        },
                        title: {
                            type: 'string',
                        },
                        description: {
                            type: 'string',
                        },
                        options: {
                            type: 'array',
                            items: {
                                type: 'object',
                                properties: {
                                    _id: {
                                        type: 'string'
                                    },
                                    title: {
                                        type: 'string'
                                    }
                                }
                            }
                        },
                        votes: {
                            type: 'array',
                            items: {
                                "$ref": '#/components/schemas/Vote',
                            }
                        },
                        created: {
                            type: 'integer'
                        },
                        starts: {
                            type: 'integer'
                        },
                        ends: {
                            type: 'integer'
                        },
                        secret: {
                            type: 'boolean',
                            default: false
                        }
                    }
                },
                Vote: {
                    type: 'object',
                    properties: {
                        _id: {
                            type: 'string',
                        },
                        hash: {
                            type: 'string',
                        }
                    }
                },
            },
        },
        security: [
            {
                "bearerAuth": []
            }
        ]
    },
    // List of files to be processes. You can also set globs './routes/*.js'
    apis: ['./src/routes/*.js'],
};

const specs = swaggerJsdoc(options)

module.exports = specs;
