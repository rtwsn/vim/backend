module.exports = async ({protocol = 'http', host = 'localhost', port = 5984, user, password, db}) => {
    const nano = require('nano')({
        url: `${protocol}://${host}:${port}`,
        requestDefaults: {
            jar:true
        }
    })
    if (user && password) {
        try {
            await nano.auth(user, password)
        } catch (e) {
            throw Error("Invalid database user")
        }
    }
    if (!db) {
        return nano
    }
    const databases = await nano.db.list()
    if (!databases.includes(db)) {
        try {
            await nano.db.create(db)
        } catch (e) {
            throw Error(`Unable to create database "${db}"`)
        }
    }
    return nano.db.use(db)
}
