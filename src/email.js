const nodemailer = require('nodemailer')

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const usernameWhitelist = /^[a-z]+$/
// TODO: Set allowed email domains
const domainWhitelist = [
    'example.com',
]

const transporter = nodemailer.createTransport({
    jsonTransport: true
})
const prefix = '[Survey] '

module.exports = {
    validate: (email) => {
        if (!email) {
            return false
        }
        if (!email.match(emailRegex)) {
            return false
        }
        const emailParts = email.split('@')
        const username = emailParts[0]
        const domain = emailParts[1]
        if (!domainWhitelist.includes(domain)){
            return false
        }
        if (!username.match(usernameWhitelist)) {
            return false
        }
        return true
    },
    send: ({ to, subject, text, html }) => {
        return new Promise((resolve, reject) => {
            // TODO: Set From address
            transporter.sendMail({
                from: '"Survey Tool" <survey@example.com>',
                to: to,
                subject: prefix + subject,
                text: text,
                html: html
            }, (err, info) => {
                if (err) {
                    return reject()
                }
                resolve(info)
            })
        })
    }
}
