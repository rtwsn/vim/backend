const crypto = require('crypto')
const express = require('express')
const asyncHandler = require('express-async-handler')
const uuid = require('uuid/v4')
const email = require('../email')
const password = require('../password')
const ApiError = require('../error').ApiError
/**
 * @swagger
 * tags:
 *   - name: auth
 *     description: Authentication module
 */
const router = express.Router()

/**
 * @swagger
 *
 * /auth/register:
 *   put:
 *     description: Register to the application
 *     tags:
 *       - auth
 *     requestBody:
 *       description: The signup form
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *                 description: Email used for login.
 *     responses:
 *       201:
 *         description: Registered
 */
router.put('/register', asyncHandler(async (req, res) => {
    if (!req.body.email) {
        throw new ApiError(400, "No e-mail address provided")
    }
    if (!email.validate(req.body.email)) {
        throw new ApiError(400, "Invalid e-mail address")
    }
    const activation_token = uuid()
    try {
        await req.db.use('users').insert({
            _id: req.body.email,
            registered: (new Date()).getTime(),
            active: false,
            activation_token: activation_token,
            permissions: [
                'survey.view',
                'survey.vote',
            ],
        })
    } catch (e) {
        throw new ApiError(400)
    }
    try {
        console.log(await email.send({
            to: req.body.email,
            subject: 'Account registration',
            text: `Someone requested an account for the anonymous survey tool on your behalf.
If it was you, please click on the following link http://localhost:3000/auth/activate/${activation_token}/
If it wasn't you, just ignore this message.`
        }))
    } catch (e) {
        throw new ApiError(400)
    }
    res.status(201).end()
}))

/**
 * @swagger
 *
 * /auth/activate:
 *   post:
 *     description: Activates an account
 *     tags:
 *       - auth
 *     requestBody:
 *       description: The activation form
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - token
 *               - password
 *             properties:
 *               token:
 *                 type: string
 *                 format: uuid
 *                 description: Token used for activation.
 *               password:
 *                 type: string
 *                 format: password
 *                 description: New password to be set for the account.
 *     responses:
 *       200:
 *         description: Activated
 *
 */
router.post('/activate', asyncHandler(async (req, res) => {
    if (!req.body.token) {
        throw new ApiError(400, "No activation token provided")
    }
    if (!req.body.password) {
        throw new ApiError(400, "No password provided")
    }
    if (!password.validate(req.body.password)) {
        throw new ApiError(400, "Password does not meet requirements (8 chars, 1 upper, 1 lower and 1 number")
    }
    let user;
    try {
        const query = await req.db.use('users').find({
            selector: {
                activation_token: { "$eq": req.body.token },
                active: { "$eq": false },
            },
        })
        if (query.docs.length < 1) {
            throw Error()
        }
        user = query.docs[0]
        if (!user) {
            throw Error()
        }
    } catch (e) {
        throw new ApiError(404)
    }
    const secret = crypto.randomBytes(2048).toString('base64')
    Object.assign(user, {
        active: true,
        password: await password.hash(req.body.password),
        voteKey: req.voteHash(secret),
    })
    try {
        console.log(await email.send({
            to: req.body.email,
            subject: 'Account activation',
            text: `Congratulations, you successfully activated your account.
One more thing. The following base64 string is your secret key.
First, do not share it with anyone, else, they will be able to vote in your name.
Second, if you lose it, you cannot vote any longer. This key is non-recoverable. Put is somewhere safe!

${secret}

Happy voting!`
        }))
    } catch (e) {
        throw new ApiError(400)
    }
    try {
        await req.db.use('users').insert(user)
    } catch (e) {
        throw new ApiError(500)
    }
    res.status(200).end()
}))

/**
 * @swagger
 *
 * /auth/login:
 *   post:
 *     description: Login to the application
 *     tags:
 *       - auth
 *     requestBody:
 *       description: The activation form
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *                 description: Email used for login.
 *               password:
 *                 type: string
 *                 format: password
 *                 description: Password used for login.
 *     responses:
 *       200:
 *         description: login
 */
router.post('/login', asyncHandler(async (req, res) => {
    if (!req.body.email) {
        throw new ApiError(400, "No email provided")
    }
    if (!req.body.password) {
        throw new ApiError(400, "No password provided")
    }
    try {
        const query = await req.db.use('users').find({
            selector: {
                _id: { "$eq": req.body.email},
                // active: { "$eq": true },
            },
        })
        let user;
        if (query.docs.length < 1) {
            // To prevent time-based brute-force attacks, we will check the hash of
            user = {
                password: "$2b$10$5k9ptltQv9uzfOe2ncIcN.1.RM0BmG5hDe84THJLl5T6ceU7zVIF2",
                active: false,
            }
        } else {
            user = query.docs[0]
        }
        if (!(await password.verify(req.body.password, user.password)) || !user.active) {
            throw Error()
        }
        res.json({
            jwt: req.jwt(user)
        })
    } catch (e) {
        throw new ApiError(403)
    }
}))

/**
 * @swagger
 *
 * /auth/whoami:
 *   get:
 *     description: Show information about the currently logged in user
 *     tags:
 *       - auth
 *     responses:
 *       200:
 *         description: Registered
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.get('/whoami', (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    res.json(req.user)
})

module.exports = router
