const crypto = require('crypto')
const express = require('express')
const asyncHandler = require('express-async-handler')
const uuid = require('uuid/v4')
const email = require('../email')
const password = require('../password')
const ApiError = require('../error').ApiError
/**
 * @swagger
 * tags:
 *   - name: vote
 *     description: Vote module
 */
const router = express.Router()

/**
 * @swagger
 *
 * /vote:
 *   put:
 *     description: Register a vote for a survey
 *     tags:
 *       - vote
 *     parameters:
 *       - in: body
 *         name: vote_request
 *         description: The request for a vote
 *         schema:
 *           type: object
 *           required:
 *             - survey
 *           properties:
 *             survey:
 *               type: string
 *     responses:
 *       201:
 *         description: Preliminary vote
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 _id:
 *                   type: string
 *                   description: ID of the preliminary vote
 *                 nonce:
 *                   type: string
 *                   description: The nonce, required to provide the challenge response
 *       404:
 *         description: Survey not found
 */
router.put('/:id/vote', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    try {
        const survey = await req.db.use('surveys').get(req.body.survey)
        if (!survey) {
            throw Error()
        }
        if (survey.ends < (new Date()).getTime()) {
            throw Error()
        }
        if (survey.starts > (new Date()).getTime()) {
            throw Error()
        }
        const nonce = crypto.randomBytes(16).toString('hex')
        const query = await req.db.use('votes').insert({
            survey: survey._id,
            nonce: nonce,
            created: (new Date()).getTime(),
        })
        if (!query.ok) {
            throw Error()
        }
        res.status(201).json({
            _id: query.id,
            nonce: nonce
        })
    } catch (e) {
        throw new ApiError(404)
    }
}))

/**
 * @swagger
 *
 * /vote/{id}:
 *   put:
 *     description: Register a vote for a survey
 *     tags:
 *       - survey
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the previously registered vote
 *       - in: body
 *         name: vote
 *         description: The final vote
 *         schema:
 *           type: object
 *           required:
 *             - option
 *             - challenge
 *           properties:
 *             option:
 *               type: string
 *             challenge:
 *               type: string
 *     responses:
 *       200:
 *         description: Final vote
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Vote'
 *       404:
 *         description: Vote not found
 */
router.get('/:id', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    let vote
    try {
        vote = await req.db.use('votes').get(req.params.id)
        if (!vote) {
            throw Error()
        }
        if (!vote.nonce) {
            throw Error()
        }
        const user = await req.db.use('users').get(req.user._id)
        if (!user) {
            throw Error()
        }
    } catch (e) {
        throw new ApiError(404)
    }
    try {
        const hash = req.voteHash(user.voteKey, nonce)
        if (req.body.challenge !== hash) {
            throw Error()
        }
        const finalVote = Object.assign(vote, {
            nonce: null,
            hash: hash,
        })
        const query = await req.db.use('votes').insert(finalVote)
        if (!query.ok) {
            throw Error()
        }
        res.status(200).end()
    } catch (e) {
        throw new ApiError(400)
    }
}))



/**
 * @swagger
 *
 * /auth/activate:
 *   post:
 *     description: Activates an account
 *     tags:
 *       - auth
 *     requestBody:
 *       description: The activation form
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - token
 *               - password
 *             properties:
 *               token:
 *                 type: string
 *                 format: uuid
 *                 description: Token used for activation.
 *               password:
 *                 type: string
 *                 format: password
 *                 description: New password to be set for the account.
 *     responses:
 *       200:
 *         description: Activated
 *
 */
router.post('/activate', asyncHandler(async (req, res) => {
    if (!req.body.token) {
        throw new ApiError(400, "No activation token provided")
    }
    if (!req.body.password) {
        throw new ApiError(400, "No password provided")
    }
    if (!password.validate(req.body.password)) {
        throw new ApiError(400, "Password does not meet requirements (8 chars, 1 upper, 1 lower and 1 number")
    }
    let user;
    try {
        const query = await req.db.use('users').find({
            selector: {
                activation_token: { "$eq": req.body.token },
                active: { "$eq": false },
            },
        })
        if (query.docs.length < 1) {
            throw Error()
        }
        user = query.docs[0]
        if (!user) {
            throw Error()
        }
    } catch (e) {
        throw new ApiError(404)
    }
    Object.assign(user, {
        active: true,
        password: await password.hash(req.body.password),
    })
    try {
        await req.db.use('users').insert(user)
    } catch (e) {
        throw new ApiError(500)
    }
    res.status(200).end()
}))


module.exports = router
