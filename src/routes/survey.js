const crypto = require('crypto')
const express = require('express')
const asyncHandler = require('express-async-handler')
const uuid = require('uuid/v4')
const email = require('../email')
const password = require('../password')
const ApiError = require('../error').ApiError
/**
 * @swagger
 * tags:
 *   - name: survey
 *     description: Survey module
 */
const router = express.Router()

/**
 * @swagger
 *
 * /survey:
 *   get:
 *     description: List all surveys
 *     tags:
 *       - survey
 *     parameters:
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *         required: false
 *         description: Limit results to the given value
 *       - in: query
 *         name: offset
 *         schema:
 *           type: integer
 *         required: false
 *       - in: query
 *         name: archived
 *         schema:
 *           type: boolean
 *           default: false
 *         required: false
 *         description: Whether to include archived surveys in the results
 *       - in: query
 *         name: ended
 *         schema:
 *           type: boolean
 *           default: false
 *         required: false
 *         description: Whether to include ended surveys in the results
 *     responses:
 *       200:
 *         description: List of surveys
 */
router.get('/', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    const selector = {
        archived: {
            "$eq": false
        },
        ends: {
            "$gte": (new Date()).getTime()
        }
    }
    if ((req.query.archived  || false) === "true") {
        selector.archived = undefined
    }
    if ((req.query.ended  || false) === "true") {
        selector.ends = undefined
    }
    try {
        const query = await req.db.use('surveys').find({
            selector: selector,
            skip: Number(req.query.offset) || 0,
            limit: Number(req.query.limit) || 10,
        })
        res.status(200).json(query.docs)
    } catch (e) {
        throw new ApiError(500, e)
    }
}))

/**
 * @swagger
 *
 * /survey:
 *   put:
 *     description: Create a new survey
 *     tags:
 *       - survey
 *     requestBody:
 *       description: The activation form
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Survey'
 *     responses:
 *       201:
 *         description: Created
 *
 */
router.put('/', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    const now = (new Date()).getTime()
    if (!req.body.title) {
        throw new ApiError(400, "No title provided")
    }
    const validOptions = []
    if (typeof req.body.options === typeof []) {
        req.body.options.forEach(option => {
            if (!option.title) {
                return
            }
            validOptions.push({
                _id: uuid(),
                title: option.title,
                votes: []
            })
        })
    }
    if (validOptions.length == 0) {
        throw new ApiError(400, "No options provided")
    }
    const survey = {
        title: req.body.title,
        description: req.body.description,
        options: validOptions,
        created: now,
        starts: Number(req.body.starts) || now,
        archived: false,
        ends: Number(req.body.ends) || (now + 1000 * 3600 * 24 * 7), // Defaults to one week
        secret: !!req.body.secret
    }
    try {
        const result = await req.db.use('surveys').insert(survey)
        res.status(201).json(Object.assign(survey, {
            _id: result.id
        }))
    } catch (e) {
        throw new ApiError(400)
    }
}))

/**
 * @swagger
 *
 * /survey/{id}:
 *   get:
 *     description: Retrieves a single survey
 *     tags:
 *       - survey
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the survey to retrieve
 *     responses:
 *       200:
 *         description: Details of the survey
 *       404:
 *         description: Survey not found
 */
router.get('/:id', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    try {
        const query = await req.db.use('surveys').get(req.params.id)
        res.status(200).json(query)
    } catch (e) {
        throw new ApiError(404)
    }
}))

/**
 * @swagger
 *
 * /survey/{id}:
 *   delete:
 *     description: Archives a survey
 *     tags:
 *       - survey
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the survey to archive
 *     responses:
 *       200:
 *         description: Sucessfully archived
 *       404:
 *         description: Survey not found
 */
router.delete('/:id', asyncHandler(async (req, res) => {
    if (!req.user) {
        throw new ApiError(403)
    }
    try {
        const survey = await req.db.use('surveys').get(req.params.id)
        if (!survey) {
            throw Error()
        }
        const query = await req.db.use('surveys').insert(Object.assign(survey, {
            archived: true
        }))
        res.status(200).end()
    } catch (e) {
        throw new ApiError(404)
    }
}))


module.exports = router
