const express = require('express')
const swaggerUi = require('swagger-ui-express')
const { ApiError } = require('./error')
;(async ({
    port,
    dbConfig
        }) => {
    const db = await require('./db')(dbConfig)
    const app = express()
    app.use((req, res, next) => {
        req.db = db
        next()
    })
    app.use('/api', require('./api'))
    app.listen(port, () => console.log(`Survey app listening on port ${port}!`))
    app.use((err, req, res, next) => {
        try {
            if (err instanceof ApiError) {
                // res.setHeader('Content-Type', 'application/json')
                if (err.message instanceof Error) {
                    throw err.message
                }
                res.status(err.status).json({
                    status: err.status,
                    message: err.message,
                })
            } else {
                throw err
            }
        } catch (err) {
            console.error(err.stack)
            res.status(500).json({
                status: 500,
                message: 'Something broke!',
            })
        }
    })
})({
    port: 3000,
    dbConfig: {
        user: 'survey',
        password: 'survey',
    },
})

