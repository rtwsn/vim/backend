const crypto = require('crypto')
const express = require('express')
const swaggerUi = require('swagger-ui-express')
const jwtMiddleware = require('express-jwt')
const jwt = require('jsonwebtoken')
const router = express.Router()

const ApiError = require('./error').ApiError
const sessionLifetime = "1d"
const jwtSecret = Buffer.from('c2VjcmV0', 'base64')
const votePublicKey = Buffer.from('c2VjcmV0', 'base64')

router.use(express.json({ type: 'application/json' }))
router.use(jwtMiddleware({
    secret: jwtSecret,
    credentialsRequired: false,
}))
router.use((req, res, next) => {
    req.jwt = (user) => jwt.sign({
        _id: user._id,
        public_key: votePublicKey
    }, jwtSecret, {
        algorithm: "HS256",
        expiresIn: sessionLifetime,
    })
    next()
})
router.use((req, res, next) => {
    req.voteHash = (secret, nonce = votePublicKey) => {
        let hash = crypto
            .createHmac('sha256', secret)
        if (nonce) {
            hash = hash.update(nonce)
        }
        return hash.digest('hex')
    }
    next()
})
const swaggerSpec = require('./swagger')

router.get('/docs/openapi.json', (req, res) => {
    res.status(200).json(swaggerSpec)
})
router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))


router.use('/auth', require('./routes/auth'))
router.use('/survey', require('./routes/survey'))

router.use((err, req, res, next) => {
    if(err.name === 'UnauthorizedError') {
        throw new ApiError(403)
    }
    throw err
})

module.exports = router
