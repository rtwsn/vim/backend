const {PublicKey, PrivateKey} = require('./keys')
const {findPrime, findPrimitiveRoot, randBetween, findPQ} = require('./utils')

async function generateElGamal(bits = 256, confidence = 5) {
    const {p, q} = await findPQ(bits)
    console.log('found primes', p, q)
    const g = (await findPrimitiveRoot(p)).modPow(2, p)
    console.log('found root')
    const x = (await randBetween(1, p.minus(1))).divide(2)
    console.log('found x')
    const h = g.modPow(x, p)
    console.log('found h')

    return new ElGamal(p, q, g, x, h, bits)
}

async function newElGamal(p, q, g, x, h, bits) {
    return new ElGamal(p, q, g, x, h, bits)
}

function ElGamal(p, q, g, x, h, bits) {
    this.p = p
    this.q = q
    this.g = g
    this.private = new PrivateKey(p, q, g, x, bits)
    this.public = new PublicKey(p, q, g, h, bits)
}

ElGamal.prototype.encrypt = function (m) {
    return this.public.encrypt(m)
}

ElGamal.prototype.decrypt = function (c1, c2) {
    return this.private.decrypt(c1, c2)
}

// ;(async () => {
//     const elgamal = await generateElGamal(128)
//     console.log('created')
//     const m = 1
//     console.log(m)
//     const lowPrimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]
//     const v1 = await elgamal.encrypt(lowPrimes[0])
//     const v2 = await elgamal.encrypt(lowPrimes[1])
//     const v3 = await elgamal.encrypt(lowPrimes[1])
//     console.log(await elgamal.decrypt(v1.c1.multiply(v2.c1).multiply(v3.c1), v1.c2.multiply(v2.c2).multiply(v3.c2)))
// })()

module.exports = {
    generateElGamal,
    newElGamal,
    findPrimitiveRoot,
    randBetween,
}
