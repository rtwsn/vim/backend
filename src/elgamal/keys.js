const {randBetween} = require('./utils')

function PrivateKey(p, q, g, x, bits) {
    this.p = p
    this.q = q
    this.g = g
    this.x = x
    this.bits = bits
}

PrivateKey.prototype.decrypt = async function (c1, c2) {
    const s = c1.modPow(this.x, this.p)
    const m = s.modInv(this.p).multiply(c2).mod(this.p)
    return m
}

function PublicKey(p, q, g, h, bits) {
    this.p = p
    this.q = q
    this.g = g
    this.h = h
    this.bits = bits
}

PublicKey.prototype.encrypt = async function (m) {
    // if (m < 0 || m > 255) {
    //     throw Error("Message must be between 0 and 255")
    // }
    const y = await randBetween(0, this.p)
    const c1 = this.g.modPow(y, this.p)
    const c2 = this.h.modPow(y, this.p).multiply(m).mod(this.p)
    return {
        c1,
        c2,
        y
    }
}

module.exports = {
    PrivateKey,
    PublicKey,
}
