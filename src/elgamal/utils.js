const crypto = require('crypto')
const bigInt = require('big-integer')

/**
 * Generates a BigInteger between min (inclusive) and max (exclusive)
 *
 * @param min Minimum number (inclusive)
 * @param max Maximum number (exclusive)
 * @returns {Promise<BigInteger>}
 */
async function randBetween(min, max) {
    const range = max.minus(min).minus(1)
    const bytes = Math.floor(range.bitLength() / 8)
    let x;
    do {
        const buf = await crypto.randomBytes(bytes)
        // buf[0] |= 0x80 // ensure first bit to be set
        x = bigInt(buf.toString('hex'), 16).add(min)
    } while (x >= max)
    return x
}

async function findPrime(bits, confidence) {
    for(;;) {
        let p = await randBetween(
            bigInt(2).pow(bits - 2),
            bigInt(2).pow(bits - 1)
        )
        if (p.isEven()) {
            p = p.minus(1)
        }
        if (!p.isProbablePrime(confidence)) {
            continue
        }
        p = p.multiply(2).add(1)
        if (p.isProbablePrime(confidence)) {
            return p
        }
    }
}

async function findPQ(n_q = 256, n_p = 3072) {
    let q, p
    for (;;) {
        q = (await randBetween(0, bigInt(2).pow(n_q))).or(1)
        if (q.isProbablePrime()) {
            break
        }
    }
    console.log('q', q)
    for (;;) {
        let k = await randBetween(0, bigInt(2).pow(n_p - n_q))
        p = q * k + 1
        if (p.isProbablePrime()) {
            break
        }
    }
    console.log('p', p)
    return {
        p, q
    }
}

async function findPrimitiveRoot(p) {
    if (p == 2) {
        return bigInt(1)
    }
    const p1 = p.minus(1)
    const p2 = p1.divide(2)
    const p3 = p1.divide(p2)
    for (;;) {
        const g = await randBetween(
            2,
            p.minus(1)
        )
        if (g.modPow(p2, p) == 1) {
            continue
        }
        if (g.modPow(p3, p) == 1) {
            continue
        }
        return g
    }
}


module.exports = {
    findPrime,
    findPrimitiveRoot,
    randBetween,
    findPQ,
}
