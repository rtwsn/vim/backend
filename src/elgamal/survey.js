const bigInt = require('big-integer')
const {findPrimitiveRoot, generateElGamal, newElGamal, randBetween} = require('./elgamal')

async function generateSurvey(options, elgamal) {
    const n = Array.isArray(options) ? options.length : options
    if (n < 1) {
        throw Error("Must provide at least 2 options")
    }
    elgamal = elgamal || await generateElGamal(16)
    const G = Array.isArray(options) ? options : await Promise.all((new Array(n).fill(0)).map(i => findPrimitiveRoot(elgamal.p)))
    return new Survey(
        elgamal,
        G
    )
}

function logBase(base, x) {
    return Math.log(x) / Math.log(base)
}

function Survey(elgamal, options) {
    this.elgamal = elgamal
    this.options = options
}

Survey.prototype.vote = async function(option) {
    if (option < 0 || option >= this.options.length) {
        throw Error("Vote must be between 0 and " + this.options.length)
    }
    const g = this.elgamal.g
    const p = this.elgamal.p
    const h = this.elgamal.public.h
    const G = this.options
    const vote = this.options[option]
    const j = option
    console.log('encrypt:', j, vote)
    // Start the vote
    const v = await this.elgamal.encrypt(vote)
    const x = v.c1
    const y = v.c2
    const r = v.y
    // Proof
    const a = (new Array(G.length)).fill(0)
    const b = (new Array(G.length)).fill(0)
    const c = (new Array(G.length)).fill(0)
    const z = (new Array(G.length)).fill(0)
    for (let i = 0; i < G.length; i++) {
        if (i == j) {
            continue
        }
        c[i] = await randBetween(0, p)
        z[i] = await randBetween(0, p)

        a[i] = g.modPow(z[i], p).multiply(x.modPow(c[i], p)).mod(p)
        b[i] = h.modPow(z[i], p).multiply(y.multiply(G[i].modInv(p)).modPow(c[i], p)).mod(p)
    }
    const w = await randBetween(0, p)
    a[j] = g.modPow(w, p)
    b[j] = h.modPow(w, p)

    return {
        x, y, a, b, c, z, r, w
    }
}

Survey.prototype.generateChallenge = async function() {
    const p = this.elgamal.p
    return randBetween(0, p)
}

Survey.prototype.calculateChallenge = async function(C, j, c, z, r, w) {
    const p = this.elgamal.p
    const sum_c = c.reduce((p, c) => p.add(c), bigInt(0))
    console.log(sum_c, p)
    c[j] = C.minus(sum_c).mod(p).add(p).mod(p)
    z[j] = w.minus(r.multiply(c[j])).mod(p).add(p).mod(p)
    return {
        c, z
    }
}

Survey.prototype.validateChallenge = async function(C, x, y, a, b, c, z) {
    const g = this.elgamal.g
    const p = this.elgamal.p
    const h = this.elgamal.public.h
    const G = this.options

    const a_ = (new Array(G.length)).fill(0)
    const b_ = (new Array(G.length)).fill(0)
    const C_ = c.reduce((p, c) => p.add(c), bigInt(0)).mod(p)

    for (let i = 0; i < G.length; i++) {
        a_[i] = g.modPow(z[i], p).multiply(x.modPow(c[i], p)).mod(p)
        b_[i] = h.modPow(z[i], p).multiply(y.multiply(G[i].modInv(p)).modPow(c[i], p)).mod(p)
    }
    console.log(a, a_)
    if (!C.eq(C_)) {
        return false
    }
    for (let i = 0; i < a.length; i++) {
        if (!a[i].eq(a_[i])) {
            return false
        }
    }
    for (let i = 0; i < b.length; i++) {
        if (!b[i].eq(b_[i])) {
            return false
        }
    }
    return true
}

Survey.prototype.combineVotes = function(votes) {
    let x = bigInt(1)
    let y = bigInt(1)
    votes.forEach(vote => {
        x = x.multiply(vote.x).mod(this.elgamal.p)
        y = y.multiply(vote.y).mod(this.elgamal.p)
    })
    return {
        x,
        y,
        n: votes.length,
    }
}

function nToSum(N, S) {
    if (N <= 1) {
        return [S]
    }
    L = []
    for (let i = 0; i < S + 1; i++) {
        L = L.concat([[i, nToSum(N - 1, S - i)]])
    }
    return L
}

function compress(n, L) {
    if (typeof L[0] === typeof 0) {
        return [n.concat(L)]
    }
    Q = []
    for (let i = 0; i < L.length; i++) {
        const x = L[i]
        Q = Q.concat(compress(n.concat([x[0]]), x[1]))
    }
    return Q
}

Survey.prototype.countVotes = async function(c1, c2, voters) {
    const m = await this.elgamal.decrypt(c1, c2)
    const G = this.options
    const p = this.elgamal.p
    comb = compress([], nToSum(G.length, voters))
    for (let i = 0; i < comb.length; i++) {
        const c = comb[i]
        res = 1
        for (let j = 0; j < G.length; j++) {
            res = G[j].modPow(bigInt(c[j]), p).multiply(res)
        }
        res = res.mod(p)
        if (res.equals(m)) {
            return c
        }
    }
}

Survey.prototype.toPython = function(j) {
    console.log('p =', this.elgamal.p.toString())
    console.log('q =', this.elgamal.q.toString())
    console.log('g =', this.elgamal.g.toString())
    console.log('h =', this.elgamal.public.h.toString())
    console.log(`G = [${this.options}]`)
    console.log('j =', j + 1)
    console.log('vote =', this.options[j].toString())

}

;(async () => {
    const survey = await generateSurvey(3)
    console.log('created')
    let j = 0
    survey.toPython(j)
    const v = await survey.vote(j)
    console.log(v)
    return
    // Send x, y, a, and b
    const C = await survey.generateChallenge()
    console.log('challenge:', C)
    // console.log(`vc.voter_III(${survey.elgamal.p}, ${survey.elgamal.p}, ${survey.elgamal.g}, ${j + 1}, ${C}, [${v.c}], [${v.z}], ${v.r}, ${v.w})`)
    // Receive C
    const p = await survey.calculateChallenge(C, j, v.c, v.z, v.r, v.w)
    console.log(p)
    // console.log(`vc.HV_IV(${survey.elgamal.p}, ${survey.elgamal.p}, ${survey.elgamal.g}, ${survey.elgamal.public.h}, [${survey.options}], ${C}, ${v.x}, ${v.y}, [${v.a}], [${v.b}], [${v.c}], [${v.z}])`)
    // return
    // Send c and z
    console.log(p)
    const valid = await survey.validateChallenge(C, v.x, v.y, v.a, v.b, p.c, p.z)
    console.log('valid:', valid)
    return
    const {x, y, n} = survey.combineVotes([
        await survey.vote(0),
        await survey.vote(0),
        await survey.vote(2),
    ])
    console.log('combined')
    console.log(await survey.countVotes(x, y, n))
})()
