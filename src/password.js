const passwordPolicy = require('password-policy')
const bcrypt = require('bcrypt')
const saltRounds = 10

module.exports = {
    validate: (password) => {
        if (!password) {
            return false
        }
        if (password.length < 8) {
            return false
        }
        if (!passwordPolicy.hasLowerCase(password)) {
            return false
        }
        if (!passwordPolicy.hasUpperCase(password)) {
            return false
        }
        if (!passwordPolicy.hasNumber(password)) {
            return false
        }
        return true
    },
    hash: (password) => {
        return bcrypt.hash(password, saltRounds)
    },
    verify: (password, hash) => {
        return bcrypt.compare(password, hash)
    }
}
