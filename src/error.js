function ApiError(status, message) {
    this.status = status
    if (message) {
        this.message = message
    } else {
        switch (status) {
            case 200:
                this.message = "OK"
                break
            case 201:
                this.message = "Created"
                break
            case 400:
                this.message = "Bad Request"
                break
            case 403:
                this.message = "Forbidden"
                break
            case 404:
                this.message = "Not found"
                break
            case 500:
                this.message = "Internal server error"
                break
        }
    }
}

module.exports = {
    ApiError: ApiError
}
